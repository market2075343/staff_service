package client

import (
	"gitlab.com/market2075343/staff_service/config"
	"gitlab.com/market2075343/staff_service/genproto/staff_service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type ServiceManagerI interface {
	// Staff Service
	StaffTariffService() staff_service.StaffTariffServiceClient
	StaffService() staff_service.StaffServiceClient
}

type grpcClients struct {
	// Staff Service
	staffTariffService staff_service.StaffTariffServiceClient
	staffService       staff_service.StaffServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {
	// Staff Microservice
	connStaffService, err := grpc.Dial(
		cfg.StaffServiceHost+cfg.StaffGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		// Staff Service
		staffTariffService: staff_service.NewStaffTariffServiceClient(connStaffService),
		staffService:       staff_service.NewStaffServiceClient(connStaffService),
	}, nil
}

// Staff Service
func (g *grpcClients) StaffTariffService() staff_service.StaffTariffServiceClient {
	return g.staffTariffService
}

func (g *grpcClients) StaffService() staff_service.StaffServiceClient {
	return g.staffService
}
