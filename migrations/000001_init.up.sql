CREATE TYPE "staff_type" AS ENUM (
  'cashier',
  'shop_assistant'
);

CREATE TYPE "tariff_type" AS ENUM (
  'fixed',
  'percent'
);

CREATE TABLE "staff_tariffs" (
  "id" uuid PRIMARY KEY,
  "name" varchar,
  "type" tariff_type,
  "amount_for_cash" numeric,
  "amount_for_card" numeric,
  "created_at" timestamp DEFAULT (now()),
  "updated_at" timestamp DEFAULT (now()),
  "deleted_at" timestamp
);

CREATE TABLE "staffs" (
  "id" uuid PRIMARY KEY,
  "branch_id" uuid,
  "tariff_id" uuid,
  "name" varchar,
  "type" staff_type,
  "balance" numeric,
  "birth_date" date,
  "username" varchar not null,
  "password" varchar not null,
  "created_at" timestamp DEFAULT (now()),
  "updated_at" timestamp DEFAULT (now()),
  "deleted_at" timestamp
);

ALTER TABLE "staffs" ADD FOREIGN KEY ("tariff_id") REFERENCES "staff_tariffs" ("id");